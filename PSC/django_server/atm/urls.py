from django.urls import path, include
import atm.views

urlpatterns = [
    path('', atm.views.atm),
]